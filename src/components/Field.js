import React from "react";
import Cell from "./Cell";
const Field = props => {
    return <div className="Field">
        {props.cell.map(cell => {
            return <Cell
                key={cell.index}
                cellNumber={cell.index}
                hasItem={cell.hasItem}
                click={() => props.open(cell.index)}
                showCell={cell.showCell}
                reset={() => props.reset()}
            ></Cell>
        })}
    </div>

}
export default Field;
